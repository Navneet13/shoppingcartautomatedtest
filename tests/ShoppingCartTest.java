import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {
	
	ShoppingCart cart;
	Product phone, phone1, hamburger;
	
	//This function runs before each test case --> setUp()
	
	@Before
	public void setUp() throws Exception{
		//1. Make a new cart
	   cart = new ShoppingCart();
	   phone = new Product("Iphone", 1500);
	}   

	@Test
	public void testCreateCart() {
		//Test: when created the cart has 0 items
		
		//1. Make a new cart (Already Done in setUp() function)

		//2.  Check number of items in the cart
		int a = cart.getItemCount();
		
		//3. do assert
		assertEquals(0,a);
		
	}
	
	@Test
	public void testEmptyCart() {
		
		//1. Make a new cart (Already Done in setUp() function)
	
		//2. Add an item to the cart
		Product phone1 = new Product("Samsung", 700);
		Product hamburger = new Product("burger", 10);
		cart.addItem(phone);
		cart.addItem(phone1);
		cart.addItem(hamburger);
		
		//3. Remove the item
		cart.empty();
		
		//4. Check number of items in the cart  === E = 0
		assertEquals(0,cart.getItemCount());
		
		
	}
	
	@Test
	public void testAddProductToCart(){
//		E01: When a new product is added, the number of items must be incremented  
//		EO2: When a new product is added, the new balance must be the sum of the previous balance plus the cost of the new produ

//      1. Make a new cart (Already Done in setUp() function)
		
//		2. Make a product (*2)
	    Product phone = new Product("iphone", 1500);
		
//		3. Check the balance of the cart before adding product --previous balance
	    double startingBalance = cart.getBalance();
	    
        //   assertEquals(0,startingBalance);
	    assertEquals(0,startingBalance,0.01);
	    
//		 4. Check number of items in the cart before adding product   -- previous number items 		      
	      int startingNumItems = cart.getItemCount();
	      assertEquals(0,startingNumItems);
	      
//		5. Add the product to the cart 
	      cart.addItem(phone);
	      
	      
//		6. Check the updated number of items in cart 
//		      -- EO Previous Balance + Price of Product
	      
	      // OPTION 1: A Reusable, maintainable , great solution
		 assertEquals(startingNumItems +1, cart.getItemCount());
		 
		 // OPTION 2: not so  Reusable, maintainable , great solution
		 // assertEquals(6,cart.getItemCount());
		 
		  // 7. Check the updated balance of the cart
		           // -- EO : Previous Balance + price of product
		 double expectedBalance = startingBalance + phone.getPrice();
		 assertEquals(expectedBalance, cart.getBalance(), 0.01);
		 
		 
	
	
	} 

	}
	
	
	// 1. Make 1 function per test
   // 2. put the @Test decorator on top of the test case
	//3. Write the code for your test case
	//4. call assertEquals() in your test case
	//5. Run the program
	//6. Look at results
	

